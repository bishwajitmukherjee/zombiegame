// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "ZombiePongGameGameMode.h"
#include "ZombiePongGameHUD.h"
#include "ZombiePongGameCharacter.h"
#include "UObject/ConstructorHelpers.h"

AZombiePongGameGameMode::AZombiePongGameGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AZombiePongGameHUD::StaticClass();
}
